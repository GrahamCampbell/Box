FROM registry.gitlab.com/grahamcampbell/php:8.3-base

ENV BOX_VERSION 4.5.1

RUN wget https://github.com/humbug/box/releases/download/$BOX_VERSION/box.phar \
    && mv box.phar /usr/local/bin/box && chmod 755 /usr/local/bin/box

ENTRYPOINT ["box"]
